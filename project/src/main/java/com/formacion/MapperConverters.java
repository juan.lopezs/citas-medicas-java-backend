package com.formacion;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.formacion.dto.CitaDTO;
import com.formacion.dto.DiagnosticoDTO;
import com.formacion.dto.MedicoDTO;
import com.formacion.dto.MedicoDTO2;
import com.formacion.dto.PacienteDTO;
import com.formacion.dto.PacienteDTO2;
import com.formacion.entity.Cita;
import com.formacion.entity.Diagnostico;
import com.formacion.entity.Medico;
import com.formacion.entity.Paciente;
import com.formacion.service.CitaService;
import com.formacion.service.DiagnosticoService;
import com.formacion.service.MedicoService;
import com.formacion.service.PacienteService;


public class MapperConverters {

	@SuppressWarnings("rawtypes")
	private static HashSet<Converter> converters = new HashSet<Converter>();

	@Autowired
	DiagnosticoService diagnosticoService;

	@Autowired
	MedicoService medicoService;

	@Autowired
	PacienteService pacienteService;

	@Autowired
	CitaService citaService;

	public MapperConverters() {
		// CitaDTO --> Cita
		Converter<CitaDTO, Cita> conv = new Converter<CitaDTO, Cita>() {

			public Cita convert(MappingContext<CitaDTO, Cita> context) {
				Cita c = new Cita();
				CitaDTO source = context.getSource();

				c.setFechaHora(source.getFechaHora());

				c.setMotivoCita(source.getMotivoCita());

				int d;
				try {
					d = source.getDiagnosticoId();
					c.setDiagnostico(diagnosticoService.findDiagnosticoById(d).get());
				} catch (Exception e) {
					c.setDiagnostico(new Diagnostico());
				}

				Medico m = medicoService.findMedicoById(source.getMedicoId()).get();
				c.setMedico(m);

				Paciente p = pacienteService.findPacienteById(source.getPacienteId()).get();
				c.setPaciente(p);

				return c;
			}
		};

		// DiagnosticoDTO --> Diagnostico
		Converter<DiagnosticoDTO, Diagnostico> conv2 = new Converter<DiagnosticoDTO, Diagnostico>() {

			@Override
			public Diagnostico convert(MappingContext<DiagnosticoDTO, Diagnostico> context) {
				DiagnosticoDTO diagnostico = context.getSource();
				Diagnostico d = new Diagnostico();

				d.setCita(citaService.findCitaById(diagnostico.getCita()).get());
				d.setEnfermedad(diagnostico.getEnfermedad());
				d.setValoracionEspecialista(diagnostico.getValoracionEspecialista());

				d.setId(diagnostico.getId());
				return d;
			}
		};

		// PacienteDTO --> Paciente
		Converter<PacienteDTO, Paciente> conv3 = new Converter<PacienteDTO, Paciente>() {

			@Override
			public Paciente convert(MappingContext<PacienteDTO, Paciente> context) {
				PacienteDTO paciente = context.getSource();
				Paciente p = new Paciente();

				p.setUsuario(paciente.getUsuario());
				p.setNombre(paciente.getNombre());
				p.setApellidos(paciente.getApellidos());
				p.setClave(paciente.getClave());

				p.setDireccion(paciente.getDireccion());
				p.setNss(paciente.getNss());
				p.setTelefono(paciente.getTelefono());
				p.setNumTarjeta(paciente.getNumTarjeta());

				Set<MedicoDTO2> medicos = paciente.getMedicos();
				if (!(medicos == null) && !medicos.isEmpty()) {
					p.setMedicos(medicos.stream().map(m -> medicoService.findMedicoById(m.getUsuario()).get())
							.collect(Collectors.toSet()));
				} else {
					p.setMedicos(new HashSet<Medico>());
				}

				Set<CitaDTO> citas = paciente.getCitas();
				if (citas != null && !citas.isEmpty()) {
					p.setCitas(citas.stream().map(c -> citaService.findCitaById(c.getId()).get())
							.collect(Collectors.toSet()));
				} else {
					p.setCitas(new HashSet<Cita>());
				}

				return p;
			}
		};

		// PacienteDTO2 --> Paciente
		Converter<PacienteDTO2, Paciente> conv4 = new Converter<PacienteDTO2, Paciente>() {

			@Override
			public Paciente convert(MappingContext<PacienteDTO2, Paciente> context) {
				PacienteDTO2 paciente = context.getSource();
				Paciente p = new Paciente();

				p.setUsuario(paciente.getUsuario());
				p.setNombre(paciente.getNombre());
				p.setApellidos(paciente.getApellidos());

				p.setDireccion(paciente.getDireccion());
				p.setNss(paciente.getNss());
				p.setTelefono(paciente.getTelefono());
				p.setNumTarjeta(paciente.getNumTarjeta());

				return p;
			}
		};

		// MedicoDTO --> Medico
		Converter<MedicoDTO, Medico> conv5 = new Converter<MedicoDTO, Medico>() {

			@Override
			public Medico convert(MappingContext<MedicoDTO, Medico> context) {
				MedicoDTO source = context.getSource();
				Medico m = new Medico();

				m.setUsuario(source.getUsuario());
				m.setNombre(source.getNombre());
				m.setApellidos(source.getApellidos());
				m.setClave(source.getClave());

				m.setNumColegiado(source.getNumColegiado());

				Set<CitaDTO> citas = source.getCitas();
				if (citas != null && !citas.isEmpty()) {
					m.setCitas(citas.stream().map(c -> citaService.findCitaById(c.getId()).get())
							.collect(Collectors.toSet()));
				} else {
					m.setCitas(new HashSet<Cita>());
				}

				Set<PacienteDTO2> pacientes = source.getPacientes();
				if (pacientes != null && !citas.isEmpty()) {
					m.setPacientes(pacientes.stream().map(p -> pacienteService.findPacienteById(p.getUsuario()).get())
							.collect(Collectors.toSet()));
				} else {
					m.setPacientes(new HashSet<Paciente>());
				}

				return m;
			}
		};

		// MedicoDTO2 --> Medico
		Converter<MedicoDTO2, Medico> conv6 = new Converter<MedicoDTO2, Medico>() {

			@Override
			public Medico convert(MappingContext<MedicoDTO2, Medico> context) {
				MedicoDTO2 source = context.getSource();
				Medico m = new Medico();

				m.setUsuario(source.getUsuario());
				m.setNombre(source.getNombre());
				m.setApellidos(source.getApellidos());

				m.setNumColegiado(source.getNumColegiado());

				return m;
			}
		};

		MapperConverters.converters.add(conv);
		MapperConverters.converters.add(conv2);
		MapperConverters.converters.add(conv3);
		MapperConverters.converters.add(conv4);
		MapperConverters.converters.add(conv5);
		MapperConverters.converters.add(conv6);
	}

	@SuppressWarnings("rawtypes")
	public HashSet<Converter> getConverters() {
		return converters;
	}

}
