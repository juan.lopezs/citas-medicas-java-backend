package com.formacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.dao.PacienteDAO;
import com.formacion.entity.Paciente;

@Service
@Transactional
public class PacienteServiceImpl implements PacienteService {

	@Autowired
	PacienteDAO pacienteDAO;

	@Override
	public List<Paciente> findAllPacientes() {
		return this.pacienteDAO.findAll();
	}

	@Override
	public Optional<Paciente> findPacienteById(String id) {
		Optional<Paciente> paciente = this.pacienteDAO.findById(id);
		return paciente;
	}

	@Override
	public Paciente savePaciente(Paciente paciente) {
		if (paciente != null) {
			Paciente p = this.pacienteDAO.save(paciente);
			return p;
		}
		return new Paciente();
	}

	@Override
	public boolean deletePaciente(String id) {
		if (this.pacienteDAO.findById(id).isPresent()) {
			this.pacienteDAO.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updatePaciente(Paciente paciente) {
		String id = paciente.getUsuario();
		if (this.pacienteDAO.findById(id).isPresent()) {
			Paciente p = this.pacienteDAO.getOne(id);

			p.setUsuario(id);
			p.setApellidos(paciente.getApellidos());
			p.setClave(paciente.getClave());
			p.setNombre(paciente.getNombre());

			p.setDireccion(paciente.getDireccion());
			p.setNumTarjeta(paciente.getNumTarjeta());
			p.setNss(paciente.getNss());
			p.setTelefono(paciente.getTelefono());

			p.setMedicos(paciente.getMedicos());
			p.setCitas(paciente.getCitas());

			this.pacienteDAO.save(paciente);

			return true;
		}
		return false;
	}

}
