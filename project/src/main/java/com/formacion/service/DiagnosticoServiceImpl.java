package com.formacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.dao.DiagnosticoDAO;
import com.formacion.entity.Diagnostico;

@Service
@Transactional
public class DiagnosticoServiceImpl implements DiagnosticoService {

	@Autowired
	DiagnosticoDAO diagnosticoDAO;

	@Override
	public List<Diagnostico> findAllDiagnosticos() {
		return diagnosticoDAO.findAll();
	}

	@Override
	public Optional<Diagnostico> findDiagnosticoById(Integer id) {
		Optional<Diagnostico> diagnostico = diagnosticoDAO.findById(id);
		return diagnostico;
	}

	@Override
	public Diagnostico saveDiagnostico(Diagnostico diagnostico) {
		if (diagnostico != null) {
			return diagnosticoDAO.save(diagnostico);
		}
		return new Diagnostico();
	}

	@Override
	public boolean deleteDiagnostico(Integer id) {
		if (diagnosticoDAO.findById(id).isPresent()) {
			diagnosticoDAO.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateDiagnostico(Diagnostico diagnostico) {
		Integer id = diagnostico.getId();
		if (diagnosticoDAO.findById(id).isPresent()) {
			Diagnostico diagnosticoNuevo = new Diagnostico();
			diagnosticoNuevo.setId(id);
			diagnosticoNuevo.setCita(diagnostico.getCita());
			diagnosticoNuevo.setEnfermedad(diagnostico.getEnfermedad());
			diagnosticoNuevo.setValoracionEspecialista(diagnostico.getValoracionEspecialista());

			diagnosticoDAO.save(diagnosticoNuevo);
			return true;
		}
		return false;
	}

}
