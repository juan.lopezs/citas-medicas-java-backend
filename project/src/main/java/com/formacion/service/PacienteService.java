package com.formacion.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.entity.Paciente;

import java.util.Optional;

@Service
public interface PacienteService {

	public List<Paciente> findAllPacientes();

	public Optional<Paciente> findPacienteById(String id);

	public Paciente savePaciente(Paciente paciente);

	public boolean deletePaciente(String id);

	public boolean updatePaciente(Paciente paciente);

}
