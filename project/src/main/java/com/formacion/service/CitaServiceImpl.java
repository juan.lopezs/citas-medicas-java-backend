package com.formacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.dao.CitaDAO;
import com.formacion.entity.Cita;

@Service
@Transactional
public class CitaServiceImpl implements CitaService {

	@Autowired
	CitaDAO citaDAO;

	@Override
	public List<Cita> findAllCitas() {
		return citaDAO.findAll();
	}

	@Override
	public Optional<Cita> findCitaById(Integer id) {
		Optional<Cita> cita = citaDAO.findById(id);
		return cita;
	}

	@Override
	public Cita saveCita(Cita cita) {
		if (cita != null) {
			return citaDAO.save(cita);
		}
		return new Cita();
	}

	@Override
	public boolean deleteCita(Integer id) {
		if (citaDAO.findById(id).isPresent()) {
			citaDAO.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCita(Cita cita) {
		Integer id = cita.getId();
		if (citaDAO.findById(id).isPresent()) {
			Cita citaNueva = new Cita();
			citaNueva.setId(id);
			citaNueva.setDiagnostico(cita.getDiagnostico());
			citaNueva.setFechaHora(cita.getFechaHora());
			citaNueva.setMedico(cita.getMedico());
			citaNueva.setMotivoCita(cita.getMotivoCita());
			citaNueva.setPaciente(cita.getPaciente());

			citaDAO.save(citaNueva);
			return true;
		}
		return false;
	}

}
