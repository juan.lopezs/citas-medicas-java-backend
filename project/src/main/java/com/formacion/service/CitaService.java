package com.formacion.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.entity.Cita;

import java.util.Optional;

@Service
public interface CitaService {

	public List<Cita> findAllCitas();

	public Optional<Cita> findCitaById(Integer id);

	public Cita saveCita(Cita cita);

	public boolean deleteCita(Integer id);

	public boolean updateCita(Cita cita);

}
