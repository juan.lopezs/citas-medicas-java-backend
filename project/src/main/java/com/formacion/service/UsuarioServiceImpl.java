package com.formacion.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.formacion.dao.UsuarioDAO;
import com.formacion.entity.Usuario;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	UsuarioDAO usuarioDAO;

	@Override
	public List<Usuario> findAllUsuarios() {
		return usuarioDAO.findAll();
	}

	@Override
	public Optional<Usuario> findUsuarioById(String id) {
		Optional<Usuario> usuario = usuarioDAO.findById(id);
		return usuario;
	}

	@Override
	public Usuario saveUsuario(Usuario usuario) {
		if (usuario != null) {
			return usuarioDAO.save(usuario);
		}
		return new Usuario();
	}

	@Override
	public boolean deleteUsuario(String id) {
		if (usuarioDAO.findById(id).isPresent()) {
			usuarioDAO.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateUsuario(Usuario usuario) {
		String id = usuario.getUsuario();
		if (usuarioDAO.findById(id).isPresent()) {
			Usuario usuarioNuevo = new Usuario();
			usuarioNuevo.setUsuario(id);
			usuarioNuevo.setApellidos(usuario.getApellidos());
			usuarioNuevo.setClave(usuario.getClave());
			usuarioNuevo.setNombre(usuario.getNombre());
			usuarioDAO.save(usuarioNuevo);
			return true;
		}
		return false;
	}

}
