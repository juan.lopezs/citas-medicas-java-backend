package com.formacion.service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.formacion.dao.MedicoDAO;
import com.formacion.entity.Medico;

@Service
@Transactional
public class MedicoServiceImpl implements MedicoService {

	@Autowired
	MedicoDAO medicoDAO;

	@Override
	public List<Medico> findAllMedicos() {
		return medicoDAO.findAll();
	}

	@Override
	public Optional<Medico> findMedicoById(String id) {
		Optional<Medico> medico = medicoDAO.findById(id);
		return medico;
	}

	@Override
	public Medico saveMedico(Medico medico) {
		if (medico != null) {
			return medicoDAO.save(medico);
		}
		return new Medico();
	}

	@Override
	public boolean deleteMedico(String id) {
		if (medicoDAO.findById(id).isPresent()) {
			medicoDAO.deleteById(id);
			return true;
		}
		return false;
	}

	@Override
	public boolean updateMedico(Medico medico) {
		String id = medico.getUsuario();
		if (medicoDAO.findById(id).isPresent()) {
			Medico medicoNuevo = new Medico();
			medicoNuevo.setUsuario(id);
			medicoNuevo.setApellidos(medico.getApellidos());
			medicoNuevo.setClave(medico.getClave());
			medicoNuevo.setNombre(medico.getNombre());
			medicoNuevo.setNumColegiado(medico.getNumColegiado());
			medicoDAO.save(medicoNuevo);
			return true;
		}
		return false;
	}

	@Override
	public Set<Medico> initializePaciente() {
		List<Medico> options = this.findAllMedicos();
		int n;
		if (options.size() < 3)
			n = options.size();
		else
			n = 3;

		Set<Medico> medicos = new HashSet<Medico>();

		for (int i = 0; i < n; i++) {
			Collections.shuffle(options);
			medicos.add(options.get(0));
			options.remove(0);
		}

		return medicos;
	}

}
