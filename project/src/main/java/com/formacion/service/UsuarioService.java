package com.formacion.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.entity.Usuario;

import java.util.Optional;

@Service
public interface UsuarioService {

	public List<Usuario> findAllUsuarios();

	public Optional<Usuario> findUsuarioById(String id);

	public Usuario saveUsuario(Usuario usuario);

	public boolean deleteUsuario(String id);

	public boolean updateUsuario(Usuario usuario);

}
