package com.formacion.service;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Service;
import com.formacion.entity.Medico;

@Service
public interface MedicoService {

	public List<Medico> findAllMedicos();

	public Optional<Medico> findMedicoById(String id);

	public Medico saveMedico(Medico medico);

	public boolean deleteMedico(String id);

	public boolean updateMedico(Medico medico);

	// Creacion de paciente
	public Set<Medico> initializePaciente();

}
