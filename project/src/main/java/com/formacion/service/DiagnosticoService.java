package com.formacion.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.formacion.entity.Diagnostico;

import java.util.Optional;

@Service
public interface DiagnosticoService {

	public List<Diagnostico> findAllDiagnosticos();

	public Optional<Diagnostico> findDiagnosticoById(Integer id);

	public Diagnostico saveDiagnostico(Diagnostico diagnostico);

	public boolean deleteDiagnostico(Integer id);

	public boolean updateDiagnostico(Diagnostico diagnostico);

}
