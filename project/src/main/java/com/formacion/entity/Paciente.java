package com.formacion.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "PACIENTE")
@PrimaryKeyJoinColumn(referencedColumnName = "usuario")
public class Paciente extends Usuario {

	@Column(name = "nss", nullable = false)
	private String nss;
	@Column(name = "numTarjeta", nullable = false)
	private String numTarjeta;
	@Column(name = "telefono", nullable = false)
	private String telefono;
	@Column(name = "direccion", nullable = false)
	private String direccion;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "paciente", orphanRemoval = true)
	private Set<Cita> citas;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "PACIENTE_MEDICO", joinColumns = {
			@JoinColumn(name = "pacienteId", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "medicoId", nullable = false) })
	private Set<Medico> medicos;

	public Paciente() {
		super();

	}

	public Paciente(String nombre, String apellidos, String usuario, String clave, String nss, String numTarjeta,
			String telefono, String direccion) {
		super(nombre, apellidos, usuario, clave);
		this.nss = nss;
		this.numTarjeta = numTarjeta;
		this.telefono = telefono;
		this.direccion = direccion;
		this.citas = new HashSet<Cita>();
		this.medicos = new HashSet<Medico>();
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Set<Cita> getCitas() {
		return citas;
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public Set<Medico> getMedicos() {
		return medicos;
	}

	public void setMedicos(Set<Medico> medicos) {
		this.medicos = medicos;
	}

	public void addCita(Cita cita) {
		citas.add(cita);
	}

	public void addMedico(Medico medico) {
		medicos.add(medico);
	}

	@Override
	public String toString() {
		return "Paciente [nss=" + nss + ", numTarjeta=" + numTarjeta + ", telefono=" + telefono + ", direccion="
				+ direccion + ", citas=" + citas.size() + ", medicos=" + medicos.size() + "]";
	}

}
