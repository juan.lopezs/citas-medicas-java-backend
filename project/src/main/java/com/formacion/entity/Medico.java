package com.formacion.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "MEDICO")
@PrimaryKeyJoinColumn(referencedColumnName = "usuario")
public class Medico extends Usuario {

	@Column(name = "numColegiado", nullable = false)
	private String numColegiado;

	@OneToMany(mappedBy = "medico", cascade = CascadeType.ALL, orphanRemoval = true)
	private Set<Cita> citas;

	@ManyToMany(mappedBy = "medicos", cascade = CascadeType.ALL)
	private Set<Paciente> pacientes;

	public Medico() {
		super();
	}

	public Medico(String nombre, String apellidos, String usuario, String clave, String numColegiado) {
		super(nombre, apellidos, usuario, clave);
		this.numColegiado = numColegiado;
		this.pacientes = new HashSet<Paciente>();
		this.citas = new HashSet<Cita>();
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public Set<Cita> getCitas() {
		return new HashSet<Cita>(this.citas);
	}

	public void setCitas(Set<Cita> citas) {
		this.citas = citas;
	}

	public void addCita(Cita cita) {
		citas.add(cita);
	}

	public Set<Paciente> getPacientes() {
		return new HashSet<Paciente>(this.pacientes);
	}

	public void setPacientes(Set<Paciente> pacientes) {
		this.pacientes = pacientes;
	}

	public void addPaciente(Paciente paciente) {
		pacientes.add(paciente);
	}

	@Override
	public String toString() {
		return "Medico [numColegiado=" + numColegiado + ", citas=" + citas.size() + ", pacientes=" + pacientes.size()
				+ "]";
	}

}
