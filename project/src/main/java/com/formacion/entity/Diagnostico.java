package com.formacion.entity;

import javax.persistence.*;

import java.io.Serializable;

@Entity
@Table(name = "DIAGNOSTICO")
public class Diagnostico implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idDiagnostico")
	private Integer id;

	@Column(name = "valoracionEspecialista")
	private String valoracionEspecialista;
	@Column(name = "enfermedad")
	private String enfermedad;

	@OneToOne(mappedBy = "diagnostico", cascade = CascadeType.PERSIST)
	private Cita cita;

	public Diagnostico(String valoracionEspecialista, String enfermedad, Cita cita) {
		this.valoracionEspecialista = valoracionEspecialista;
		this.enfermedad = enfermedad;
		this.cita = cita;
	}

	public Diagnostico() {

	}

	/**
	 * Getters y setters
	 */
	public String getValoracionEspecialista() {
		return valoracionEspecialista;
	}

	public void setValoracionEspecialista(String valoracionEspecialista) {
		this.valoracionEspecialista = valoracionEspecialista;
	}

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public Cita getCita() {
		return cita;
	}

	public void setCita(Cita cita) {
		this.cita = cita;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

}
