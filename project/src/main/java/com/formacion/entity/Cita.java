package com.formacion.entity;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "CITA")
public class Cita implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fechaHora")
	private Date fechaHora;

	@Column(name = "motivoCita", nullable = false)
	private String motivoCita;

	// Objetos presentes en relaciones
	@ManyToOne
	@JoinColumn(name = "pacienteId", nullable = false, updatable = true)
	private Paciente paciente;

	@ManyToOne
	@JoinColumn(name = "medicoId", nullable = false, updatable = true)
	private Medico medico;

	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "diagnosticoId", referencedColumnName = "idDiagnostico", nullable = true)
	private Diagnostico diagnostico;

	public Cita(Date fechaHora, String motivoCita, Paciente paciente, Medico medico, Diagnostico diagnostico) {
		this.fechaHora = fechaHora;
		this.motivoCita = motivoCita;
		this.paciente = paciente;
		this.medico = medico;
		this.diagnostico = diagnostico;
	}

	public Cita() {

	}

	/**
	 * Getters y setters
	 */
	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public Integer getId() {
		return id;
	}

	public Paciente getPaciente() {
		return paciente;
	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Diagnostico getDiagnostico() {
		return diagnostico;
	}

	public void setDiagnostico(Diagnostico diagnostico) {
		this.diagnostico = diagnostico;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Cita [id=" + id + ", fechaHora=" + fechaHora + ", motivoCita=" + motivoCita + ", paciente="
				+ paciente.getUsuario() + ", medico=" + medico.getUsuario() + ", diagnostico=" + diagnostico + "]";
	}

}
