package com.formacion.restcontroller;

import java.util.Optional;

import com.formacion.dto.UsuarioDTO;

public interface UsuarioRest {
	@SuppressWarnings("rawtypes")
	public Optional login(UsuarioDTO usuario);

}
