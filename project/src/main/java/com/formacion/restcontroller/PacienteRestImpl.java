package com.formacion.restcontroller;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.dto.PacienteDTO;
import com.formacion.dto.PacienteDTO2;
import com.formacion.entity.Medico;
import com.formacion.entity.Paciente;
import com.formacion.entity.Usuario;
import com.formacion.service.MedicoService;
import com.formacion.service.PacienteService;
import com.formacion.service.UsuarioService;

@RestController
@RequestMapping("api/pacientes")
@CrossOrigin(origins = "http://localhost:4200")
public class PacienteRestImpl implements PacienteRest {

	@Autowired
	PacienteService pacienteService;

	@Autowired
	UsuarioService userService;

	@Autowired
	MedicoService medicoService;

	@Autowired
	ModelMapper modelMapper;

	@GetMapping
	public ResponseEntity<List<PacienteDTO>> getPacientes() {
		List<Paciente> pacientes = this.pacienteService.findAllPacientes();
		return ResponseEntity.ok(pacientes.stream().map(p -> new PacienteDTO(p)).collect(Collectors.toList()));
	}

	@GetMapping("/{id}")
	public ResponseEntity<PacienteDTO> getPacienteById(@PathVariable(value = "id") String pacienteId) {
		Optional<Paciente> paciente = this.pacienteService.findPacienteById(pacienteId);
		if (paciente.isPresent()) {
			System.out.println(paciente.get());
			return ResponseEntity.ok(new PacienteDTO(paciente.get()));
		}
		return ResponseEntity.notFound().build();
	}

	@PostMapping
	public ResponseEntity<PacienteDTO2> addPaciente(@RequestBody PacienteDTO paciente) {
		Paciente p = modelMapper.map(paciente, Paciente.class);

		Optional<Usuario> u = this.userService.findUsuarioById(paciente.getUsuario());
		if (u.isPresent()) {
			return ResponseEntity.status(409).build(); // Error ya registrado
		}

		Set<Medico> m = medicoService.initializePaciente();
		p.setMedicos(m);

		Paciente p2 = this.pacienteService.savePaciente(p);
		return ResponseEntity.ok(new PacienteDTO2(p2));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> deletePaciente(@PathVariable(value = "id") String pacienteId) {
		if (this.pacienteService.deletePaciente(pacienteId))
			return ResponseEntity.ok("Paciente eliminado");
		return ResponseEntity.notFound().build();
	}

	@PutMapping
	public ResponseEntity<String> updatePaciente(@RequestBody PacienteDTO pacienteNuevo) {
		Paciente p = modelMapper.map(pacienteNuevo, Paciente.class);

		if (this.pacienteService.updatePaciente(p)) {
			return ResponseEntity.ok("Paciente actualizado");

		}

		return ResponseEntity.notFound().build();
	}
}
