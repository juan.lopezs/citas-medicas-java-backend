package com.formacion.restcontroller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.dto.DiagnosticoDTO;
import com.formacion.entity.Diagnostico;
import com.formacion.service.DiagnosticoService;

@RestController
@RequestMapping("api/diagnosticos")
@CrossOrigin(origins = "http://localhost:4200")
public class DiagnosticoRestImpl implements DiagnosticoRest {

	@Autowired
	DiagnosticoService diagnosticoService;

	@Autowired
	ModelMapper modelMapper;

	// get all
	@GetMapping
	public ResponseEntity<List<DiagnosticoDTO>> getDiagnosticos() {
		List<Diagnostico> diagnosticos = this.diagnosticoService.findAllDiagnosticos();
		return ResponseEntity.ok(diagnosticos.stream().map(d -> new DiagnosticoDTO(d)).collect(Collectors.toList()));
	}

	// get id
	@GetMapping("/{id}")
	public ResponseEntity<DiagnosticoDTO> getDiagnosticoById(@PathVariable(value = "id") Integer diagnosticoId) {
		Optional<Diagnostico> diagnostico = this.diagnosticoService.findDiagnosticoById(diagnosticoId);
		if (diagnostico.isPresent()) {
			return ResponseEntity.ok(new DiagnosticoDTO(diagnostico.get()));
		}
		return ResponseEntity.notFound().build();
	}

	// create
	@PostMapping
	public ResponseEntity<DiagnosticoDTO> addDiagnostico(@RequestBody DiagnosticoDTO diagnostico) {

		Optional<Diagnostico> d = this.diagnosticoService.findDiagnosticoById(diagnostico.getId());
		if (d.isPresent()) {
			return ResponseEntity.status(409).build(); // Error ya registrado
		}

		Diagnostico d2 = modelMapper.map(diagnostico, Diagnostico.class);
		Diagnostico d3 = this.diagnosticoService.saveDiagnostico(d2);

		return ResponseEntity.ok(new DiagnosticoDTO(d3));
	}

	// update
	@PutMapping
	public ResponseEntity<String> updateDiagnostico(@RequestBody DiagnosticoDTO diagnostico) {
		System.out.println(diagnostico);
		Diagnostico d = modelMapper.map(diagnostico, Diagnostico.class);
		System.out.println(d.toString());
		if (this.diagnosticoService.updateDiagnostico(d))
			return ResponseEntity.ok("Diagnostico actualizado");
		return ResponseEntity.notFound().build();
	}

	// delete
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteDiagnostico(@PathVariable(value = "id") Integer diagnosticoId) {
		if (this.diagnosticoService.deleteDiagnostico(diagnosticoId))
			return ResponseEntity.ok("Diagnostico eliminado");
		return ResponseEntity.notFound().build();
	}
}
