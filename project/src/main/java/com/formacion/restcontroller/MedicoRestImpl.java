package com.formacion.restcontroller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.dto.MedicoDTO;
import com.formacion.dto.MedicoDTO2;
import com.formacion.entity.Medico;
import com.formacion.entity.Usuario;
import com.formacion.service.MedicoService;
import com.formacion.service.UsuarioService;

@RestController
@RequestMapping("/api/medicos")
@CrossOrigin(origins = "http://localhost:4200")
public class MedicoRestImpl implements MedicoRest {

	@Autowired
	MedicoService medicoService;

	@Autowired
	UsuarioService userService;

	@Autowired
	ModelMapper modelMapper;

	// get all
	@GetMapping
	public ResponseEntity<List<MedicoDTO>> getMedicos() {
		List<Medico> medicos = this.medicoService.findAllMedicos();
		return ResponseEntity.ok(medicos.stream().map(m -> new MedicoDTO(m)).collect(Collectors.toList()));
	}

	// get id
	@GetMapping("/{id}")
	public ResponseEntity<MedicoDTO> getMedicoById(@PathVariable(value = "id") String medicoId) {
		Optional<Medico> medico = this.medicoService.findMedicoById(medicoId);
		if (medico.isPresent()) {
			return ResponseEntity.ok(modelMapper.map(medico.get(), MedicoDTO.class));
		}
		return ResponseEntity.notFound().build();

	}

	// create
	@PostMapping
	public ResponseEntity<MedicoDTO2> addMedico(@RequestBody MedicoDTO medico) {

		Medico m = modelMapper.map(medico, Medico.class);

		Optional<Usuario> u = this.userService.findUsuarioById(medico.getUsuario());
		if (u.isPresent()) {
			return ResponseEntity.ok(new MedicoDTO2());
		}

		Medico m2 = this.medicoService.saveMedico(m);

		return ResponseEntity.ok(new MedicoDTO2(m2));
	}

	// delete
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteMedico(@PathVariable(value = "id") String medicoId) {
		if (this.medicoService.deleteMedico(medicoId))
			return ResponseEntity.ok("Medico eliminado");
		return ResponseEntity.notFound().build();
	}

	// update
	@PutMapping
	public ResponseEntity<String> updateMedico(@RequestBody MedicoDTO medicoNuevo) {
		Medico m = modelMapper.map(medicoNuevo, Medico.class);
		if (this.medicoService.updateMedico(m))
			return ResponseEntity.ok("Medico actualizado");
		return ResponseEntity.notFound().build();
	}

}
