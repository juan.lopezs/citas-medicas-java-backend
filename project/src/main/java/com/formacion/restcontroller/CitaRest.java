package com.formacion.restcontroller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.formacion.dto.CitaDTO;

public interface CitaRest {

	public ResponseEntity<List<CitaDTO>> getCitas();

	public ResponseEntity<CitaDTO> getCitaById(Integer id);

	public ResponseEntity<CitaDTO> addCita(CitaDTO cita);

	public ResponseEntity<String> deleteCita(Integer cita);

	public ResponseEntity<String> updateCita(CitaDTO citaNueva);

}
