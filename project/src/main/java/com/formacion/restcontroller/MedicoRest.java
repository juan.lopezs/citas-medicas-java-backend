package com.formacion.restcontroller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.formacion.dto.MedicoDTO;
import com.formacion.dto.MedicoDTO2;

public interface MedicoRest {

	public ResponseEntity<List<MedicoDTO>> getMedicos();

	public ResponseEntity<MedicoDTO> getMedicoById(String id);

	public ResponseEntity<MedicoDTO2> addMedico(MedicoDTO medico);

	public ResponseEntity<String> deleteMedico(String medico);

	public ResponseEntity<String> updateMedico(MedicoDTO medicoNuevo);

}
