package com.formacion.restcontroller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.formacion.dto.PacienteDTO;
import com.formacion.dto.PacienteDTO2;

public interface PacienteRest {

	public ResponseEntity<List<PacienteDTO>> getPacientes();

	public ResponseEntity<PacienteDTO> getPacienteById(String pacienteId);

	public ResponseEntity<PacienteDTO2> addPaciente(PacienteDTO paciente);

	public ResponseEntity<String> deletePaciente(String pacienteId);

	public ResponseEntity<String> updatePaciente(PacienteDTO paciente);

}
