package com.formacion.restcontroller;

import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.dto.MedicoDTO;
import com.formacion.dto.PacienteDTO;
import com.formacion.dto.UsuarioDTO;
import com.formacion.entity.Medico;
import com.formacion.entity.Paciente;
import com.formacion.entity.Usuario;
import com.formacion.service.UsuarioService;

@RestController
@RequestMapping("api/usuarios")
@CrossOrigin(origins = "http://localhost:4200")
public class UsuarioRestImpl implements UsuarioRest {

	@Autowired
	UsuarioService usuarioService;

	@Autowired
	ModelMapper modelMapper;

	@SuppressWarnings("rawtypes")
	@PostMapping
	public Optional login(@RequestBody UsuarioDTO usuario) {
		Optional<Usuario> u = this.usuarioService.findUsuarioById(usuario.getUsuario());

		try {
			if (u.isPresent() && u.get().getClave().equals(usuario.getClave())) {
				if (u.get() instanceof Medico) {
					return Optional.of(new MedicoDTO((Medico) u.get()));
				} else {
					return Optional.of(new PacienteDTO((Paciente) u.get()));
				}
			}

		} catch (Exception e) {

		}
		return Optional.of(new UsuarioDTO());
	}

}
