package com.formacion.restcontroller;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.formacion.dto.DiagnosticoDTO;

public interface DiagnosticoRest {

	public ResponseEntity<List<DiagnosticoDTO>> getDiagnosticos();

	public ResponseEntity<DiagnosticoDTO> getDiagnosticoById(Integer id);

	public ResponseEntity<DiagnosticoDTO> addDiagnostico(DiagnosticoDTO diagnostico);

	public ResponseEntity<String> deleteDiagnostico(Integer diagnostico);

	public ResponseEntity<String> updateDiagnostico(DiagnosticoDTO diagnosticoNuevo);

}
