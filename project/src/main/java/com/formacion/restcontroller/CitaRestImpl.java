package com.formacion.restcontroller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.formacion.dto.CitaDTO;
import com.formacion.entity.Cita;
import com.formacion.service.CitaService;

@RestController
@RequestMapping("api/citas")
@CrossOrigin(origins = "http://localhost:4200")
public class CitaRestImpl implements CitaRest {

	@Autowired
	CitaService citaService;

	@Autowired
	ModelMapper modelMapper;

	// get all
	@GetMapping
	public ResponseEntity<List<CitaDTO>> getCitas() {
		List<Cita> citas = this.citaService.findAllCitas();
		return ResponseEntity.ok(citas.stream().map(c -> new CitaDTO(c)).collect(Collectors.toList()));
	}

	// get id
	@GetMapping("/{id}")
	public ResponseEntity<CitaDTO> getCitaById(@PathVariable(value = "id") Integer citaId) {
		Optional<Cita> cita = this.citaService.findCitaById(citaId);
		if (cita.isPresent()) {
			return ResponseEntity.ok(modelMapper.map(cita, CitaDTO.class));
		}
		return ResponseEntity.notFound().build();
	}

	// create
	@PostMapping
	public ResponseEntity<CitaDTO> addCita(@RequestBody CitaDTO cita) {

		if (cita.getId() != null) {
			Optional<Cita> c = this.citaService.findCitaById(cita.getId());
			if (c.isPresent()) {
				return ResponseEntity.notFound().build(); // Error ya registrado
			}
		}

		Cita c2 = modelMapper.map(cita, Cita.class);
		Cita c3 = this.citaService.saveCita(c2);
		return ResponseEntity.ok(new CitaDTO(c3));
	}

	// delete
	@DeleteMapping("/{id}")
	public ResponseEntity<String> deleteCita(@PathVariable(value = "id") Integer citaId) {
		if (this.citaService.deleteCita(citaId))
			return ResponseEntity.ok("Cita eliminada");
		return ResponseEntity.notFound().build();
	}

	// update
	@PutMapping
	public ResponseEntity<String> updateCita(@RequestBody CitaDTO cita) {
		Cita c = modelMapper.map(cita, Cita.class);
		if (this.citaService.updateCita(c))
			return ResponseEntity.ok("Cita actualizada");
		return ResponseEntity.notFound().build();
	}
}
