package com.formacion.dto;

import com.formacion.entity.Medico;

// Used in PacienteDTO and CitaDTOs
public class MedicoDTO2 {

	private String nombre;
	private String apellidos;
	private String usuario;

	private String numColegiado;

	public MedicoDTO2() {

	}

	public MedicoDTO2(Medico medico) {
		super();
		this.nombre = medico.getNombre();
		this.apellidos = medico.getApellidos();
		this.usuario = medico.getUsuario();
		this.numColegiado = medico.getNumColegiado();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

}
