package com.formacion.dto;

import java.util.Date;

import com.formacion.entity.Cita;

public class CitaDTO {

	private Integer id;

	private Date fechaHora;

	private String motivoCita;

	private String pacienteId;

	private String medicoId;

	private Integer diagnosticoId;

	public CitaDTO(Integer id, Date fechaHora, String motivoCita, String pacienteId, String medicoId,
			Integer diagnosticoId) {
		super();
		this.id = id;
		this.fechaHora = fechaHora;
		this.motivoCita = motivoCita;
		this.pacienteId = pacienteId;
		this.medicoId = medicoId;
		this.diagnosticoId = diagnosticoId;
	}

	public CitaDTO(Cita cita) {
		this.id = cita.getId();
		this.fechaHora = cita.getFechaHora();
		this.motivoCita = cita.getMotivoCita();
		this.pacienteId = cita.getPaciente().getUsuario();
		this.medicoId = cita.getMedico().getUsuario();
		this.diagnosticoId = cita.getDiagnostico().getId();
	}

	public CitaDTO() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFechaHora() {
		return fechaHora;
	}

	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}

	public String getMotivoCita() {
		return motivoCita;
	}

	public void setMotivoCita(String motivoCita) {
		this.motivoCita = motivoCita;
	}

	public String getPacienteId() {
		return pacienteId;
	}

	public void setPacienteId(String pacienteId) {
		this.pacienteId = pacienteId;
	}

	public String getMedicoId() {
		return medicoId;
	}

	public void setMedicoId(String medicoId) {
		this.medicoId = medicoId;
	}

	public Integer getDiagnosticoId() {
		return diagnosticoId;
	}

	public void setDiagnosticoId(Integer diagnosticoId) {
		this.diagnosticoId = diagnosticoId;
	}

	@Override
	public String toString() {
		return "CitaDTO [id=" + id + ", fechaHora=" + fechaHora + ", motivoCita=" + motivoCita + ", pacienteId="
				+ pacienteId + ", medicoId=" + medicoId + ", diagnosticoId=" + diagnosticoId + "]";
	}

}
