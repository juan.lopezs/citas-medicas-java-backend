package com.formacion.dto;

import com.formacion.entity.Diagnostico;

public class DiagnosticoDTO {

	private Integer diagnosticoId;
	private String valoracionEspecialista;
	private String enfermedad;
	private Integer cita;

	public DiagnosticoDTO(Integer id, String valoracionEspecialista, String enfermedad, Integer cita) {
		super();
		this.diagnosticoId = id;
		this.valoracionEspecialista = valoracionEspecialista;
		this.enfermedad = enfermedad;
		this.cita = cita;
	}

	public DiagnosticoDTO(Diagnostico diagnostico) {
		this.diagnosticoId = diagnostico.getId();
		this.valoracionEspecialista = diagnostico.getValoracionEspecialista();
		this.enfermedad = diagnostico.getEnfermedad();
		this.cita = diagnostico.getCita().getId();
	}

	public DiagnosticoDTO() {

	}

	public Integer getId() {
		return diagnosticoId;
	}

	public void setId(Integer id) {
		this.diagnosticoId = id;
	}

	public String getValoracionEspecialista() {
		return valoracionEspecialista;
	}

	public void setValoracionEspecialista(String valoracionEspecialista) {
		this.valoracionEspecialista = valoracionEspecialista;
	}

	public String getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		this.enfermedad = enfermedad;
	}

	public Integer getCita() {
		return cita;
	}

	public void setCita(Integer cita) {
		this.cita = cita;
	}

}
