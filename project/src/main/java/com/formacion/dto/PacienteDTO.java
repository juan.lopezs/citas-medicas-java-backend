package com.formacion.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.formacion.entity.Paciente;

public class PacienteDTO {

	private String nombre;
	private String apellidos;
	private String usuario;
	private String clave;

	private String nss;
	private String telefono;
	private String direccion;
	private String numTarjeta;

	private Set<MedicoDTO2> medicos;
	private Set<CitaDTO> citas;

	public PacienteDTO(String nombre, String apellidos, String usuario, String clave, String nss, String telefono,
			String direccion, String numTarjeta, Set<CitaDTO> citas, Set<MedicoDTO2> medicos) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.usuario = usuario;
		this.clave = clave;
		this.nss = nss;
		this.telefono = telefono;
		this.direccion = direccion;
		this.numTarjeta = numTarjeta;
		this.citas = citas;
		this.medicos = medicos;
	}

	public PacienteDTO(Paciente paciente) {

		this.nombre = paciente.getNombre();
		this.apellidos = paciente.getApellidos();
		this.usuario = paciente.getUsuario();
		this.clave = paciente.getClave();
		this.numTarjeta = paciente.getNumTarjeta();
		this.nss = paciente.getNss();
		this.telefono = paciente.getTelefono();
		this.direccion = paciente.getDireccion();

		if (paciente.getCitas() != null) {
			this.citas = paciente.getCitas().stream().map(c -> new CitaDTO(c)).collect(Collectors.toSet());
		} else {
			this.citas = new HashSet<CitaDTO>();
		}

		if (paciente.getMedicos() != null) {
			this.medicos = paciente.getMedicos().stream().map(m -> new MedicoDTO2(m)).collect(Collectors.toSet());
		} else {
			this.medicos = new HashSet<MedicoDTO2>();
		}
	}

	public PacienteDTO() {

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Set<CitaDTO> getCitas() {
		return citas;
	}

	public void setCitas(Set<CitaDTO> citas) {
		this.citas = citas;
	}

	public Set<MedicoDTO2> getMedicos() {
		return medicos;
	}

	public void setMedicos(Set<MedicoDTO2> medicos) {
		this.medicos = medicos;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

}
