package com.formacion.dto;

import com.formacion.entity.Usuario;

public class UsuarioDTO {

	private String usuario;
	private String clave;

	public UsuarioDTO(String usuario, String clave) {
		super();
		this.usuario = usuario;
		this.clave = clave;
	}

	public UsuarioDTO(Usuario u) {
		this.usuario = u.getUsuario();
		this.clave = u.getClave();
	}

	public UsuarioDTO() {

	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

}
