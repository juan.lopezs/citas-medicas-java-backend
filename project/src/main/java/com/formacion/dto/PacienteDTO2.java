package com.formacion.dto;

import java.util.List;

import com.formacion.entity.Paciente;

// Used in MedicoDTO and CitaDTO
public class PacienteDTO2 {

	private String usuario;
	private String nombre;
	private String apellidos;

	private String nss;
	private String numTarjeta;
	private String telefono;
	private String direccion;

	public PacienteDTO2(String nombre, String apellidos, String usuario, String clave, String nss, String numTarjeta,
			String telefono, String direccion, List<Integer> citas, List<String> medicos) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.usuario = usuario;
		this.nss = nss;
		this.numTarjeta = numTarjeta;
		this.telefono = telefono;
		this.direccion = direccion;
	}

	public PacienteDTO2() {

	}

	public PacienteDTO2(Paciente p) {
		this.nombre = p.getNombre();
		this.apellidos = p.getApellidos();
		this.usuario = p.getUsuario();
		this.nss = p.getNss();
		this.numTarjeta = p.getNumTarjeta();
		this.telefono = p.getTelefono();
		this.direccion = p.getDireccion();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNss() {
		return nss;
	}

	public void setNss(String nss) {
		this.nss = nss;
	}

	public String getNumTarjeta() {
		return numTarjeta;
	}

	public void setNumTarjeta(String numTarjeta) {
		this.numTarjeta = numTarjeta;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
}
