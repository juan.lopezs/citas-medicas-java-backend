package com.formacion.dto;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import com.formacion.entity.Cita;
import com.formacion.entity.Medico;
import com.formacion.entity.Paciente;

public class MedicoDTO {

	private String nombre;
	private String apellidos;
	private String usuario;
	private String clave;

	private String numColegiado;

	private Set<CitaDTO> citas;
	private Set<PacienteDTO2> pacientes;

	public MedicoDTO() {

	}

	public MedicoDTO(String nombre, String apellidos, String usuario, String clave, String numColegiado,
			Set<CitaDTO> citas, Set<PacienteDTO2> pacientes) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.usuario = usuario;
		this.clave = clave;
		this.numColegiado = numColegiado;
		this.citas = citas;
		this.pacientes = pacientes;
	}

	public MedicoDTO(Medico medico) {
		super();
		this.nombre = medico.getNombre();
		this.apellidos = medico.getApellidos();
		this.usuario = medico.getUsuario();
		this.clave = medico.getClave();
		this.numColegiado = medico.getNumColegiado();

		Set<Cita> citas = medico.getCitas();

		if (citas != null) {
			this.citas = citas.stream().map(c -> new CitaDTO(c)).collect(Collectors.toSet());
		} else {
			this.citas = new HashSet<CitaDTO>();
		}

		Set<Paciente> pacientes = medico.getPacientes();
		if (pacientes != null) {
			this.pacientes = pacientes.stream().map(p -> new PacienteDTO2(p)).collect(Collectors.toSet());
		} else {
			this.pacientes = new HashSet<PacienteDTO2>();
		}

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getNumColegiado() {
		return numColegiado;
	}

	public void setNumColegiado(String numColegiado) {
		this.numColegiado = numColegiado;
	}

	public Set<CitaDTO> getCitas() {
		return citas;
	}

	public void setCitas(Set<CitaDTO> citas) {
		this.citas = citas;
	}

	public Set<PacienteDTO2> getPacientes() {
		return pacientes;
	}

	public void setPacientes(Set<PacienteDTO2> pacientes) {
		this.pacientes = pacientes;
	}

}
