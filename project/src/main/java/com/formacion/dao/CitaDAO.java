package com.formacion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.entity.Cita;

@Repository
public interface CitaDAO extends JpaRepository<Cita, Integer> {

}
