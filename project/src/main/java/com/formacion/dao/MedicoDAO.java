package com.formacion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.entity.Medico;

@Repository
public interface MedicoDAO extends JpaRepository<Medico, String> {

}
