package com.formacion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.entity.Diagnostico;

@Repository
public interface DiagnosticoDAO extends JpaRepository<Diagnostico, Integer> {

}
