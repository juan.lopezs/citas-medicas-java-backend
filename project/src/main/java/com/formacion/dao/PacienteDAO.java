package com.formacion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.formacion.entity.Paciente;

@Repository
public interface PacienteDAO extends JpaRepository<Paciente, String> {

}
